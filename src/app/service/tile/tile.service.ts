import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Tile } from '../../models/tile.model';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class TileService {

  private baseColor = '#EEEEEE';
  private tileArray: Tile[];

  /**
   */
  constructor() {
    // TODO
  }

  public getTile(): Tile[] {
    // TODO
    return [{
      color: this.baseColor,
      cols: 1,
      rows: 1,
      text: '',
    }];
  }

  public initTileForExample(): Tile[] {
    return [
      { text: '', cols: 4, rows: 4, color: this.baseColor },
      { text: '', cols: 4, rows: 4, color: this.baseColor },
      { text: '', cols: 4, rows: 4, color: this.baseColor },
      { text: '', cols: 4, rows: 4, color: this.baseColor },
    ];
  }
}
