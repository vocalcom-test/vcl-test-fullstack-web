import { Component, OnInit, ViewChild } from '@angular/core';
import { TileService } from '../../service/tile/tile.service';
import { Tile } from '../../models/tile.model';

@Component({
  selector: 'app-grid-color',
  templateUrl: './grid-color.component.html',
  styleUrls: ['./grid-color.component.scss']
})
export class GridColorComponent implements OnInit {

  // number of columns on the grids
  columns = 10;
  tileHeight = '100px';

  tiles: Tile[];

  constructor(public tileService: TileService) {

  }

  ngOnInit() {
    this.tiles = this.tileService.initTileForExample();
  }

}
